public class Panda {
	
	//Attributes//
	
	public String origin;
	public int population;
	public double lifespan;
	
	//Constructor//
	
	public Panda(String origin, int population, double lifespan) {
		this.origin = origin;
		this.population = population;
		this.lifespan = lifespan;
	}
	
	//Creating the first method//
	
	public String myIntro() {
		return "Welcome to Panda Club House! We're a species found in "
				+ origin + ". We've a population of "
				+ population +
				" Our life span is " + lifespan + " years";
	}
	
	//Creating the second method//
	
	public void newLocation() {
		origin = "Shaanxi";
	}
}
