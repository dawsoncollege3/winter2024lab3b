//Import Command//

import java.util.Scanner;

public class VirtualPetApp {
	public static void main(String[] args) { //Main Method//
		
		//Connecting to the keyboard//
		
		Scanner keyboard = new Scanner(System.in);
		
		Panda[] embarrassmentOfPanda =	new Panda[4];
		
		for(int i = 0; i < embarrassmentOfPanda.length; i++) {
			System.out.println("Enter the origin:");
			String location = keyboard.nextLine();
			
			System.out.println("Enter the population:");
			int count = keyboard.nextInt();
			keyboard.nextLine();
		
			System.out.println("Enter the lifespan:");
			double age = keyboard.nextDouble();
			keyboard.nextLine();
			
			embarrassmentOfPanda[i] = new Panda(location, count, age);
		}
		
		/* Constructor call
		 * At [] 
		 * Index = 3
		 */
		
		System.out.println(embarrassmentOfPanda[embarrassmentOfPanda.length-1].origin);
		System.out.println(embarrassmentOfPanda[embarrassmentOfPanda.length-1].population);
		System.out.println(embarrassmentOfPanda[embarrassmentOfPanda.length-1].lifespan);
		
		/* First & Second Method
		 * Call on []
		 * At index = 0;
		 */
		
		embarrassmentOfPanda[0].myIntro();
		embarrassmentOfPanda[0].newLocation();
	}
}
